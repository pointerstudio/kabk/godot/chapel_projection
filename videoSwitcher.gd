extends VideoStreamPlayer

var videos : Array = [
	"alex-r.ogv",
	"ana_again.ogv",
	"deyan.ogv",
	"domo_final.ogv",
	"Dongha-ProjectWeek.ogv",
	"FLOORanimation-Wreverse.ogv",
	"Jin.ogv",
	"klaudia.ogv",
	"michalina1.ogv",
	"michalina2.ogv",
	"olivtrash.ogv",
	"romanceanddp.ogv",
	"sasha_ALL.ogv",
	"sasha_maybe1.ogv",
	"sasha_no1.ogv",
	"sasha_yes1.ogv",
	"ScreenRecording2024-01-18at10.59.13.ogv",
	"ScreenRecording2024-01-18at16.16.09.ogv",
	"Stanley_final.ogv",
	"Stay.ogv",
	"Teun_Projection.ogv",
	"Video_Belinda.ogv",
	"Video_NC.ogv"
	]
	
var playableVideos : Array = []

func playVideo(file):
	stop()
	var vsf = "res://videos/" + file
	var video_stream_theora = VideoStreamTheora.new()
	video_stream_theora.file = vsf
	stream = video_stream_theora
	play()
	%AveragePlayer.stop()
	var vsfa = "res://averagevideos/" + file
	var video_stream_theoraa = VideoStreamTheora.new()
	video_stream_theoraa.file = vsfa
	%AveragePlayer.stream = video_stream_theoraa
	%AveragePlayer.play()

func playRandomVideo():
	if playableVideos.size() == 0:
		playableVideos = videos.duplicate(true)
	
	var index = randi() % playableVideos.size()
	var file = playableVideos[index]
	playVideo(file)
	playableVideos.remove_at(index)
	
# Called when the node enters the scene tree for the first time.
func _ready():
	#playRandomVideo()
	playVideo("FLOORanimation-Wreverse.ogv")
	
	var _image = %AveragePlayer.get_parent().get_texture().get_image()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	#pass
	var image = %AveragePlayer.get_parent().get_texture().get_image()
	#var rgb = [0,0,0]
	#
	#var pixels = [
		##image.get_pixel(0,0),
		##image.get_pixel(1920-50,50),
		##image.get_pixel(1920/2.0,1080/2.0),
		##image.get_pixel(1920-50,1080-50),
		##image.get_pixel(50,1080-50)
	#]
	#var howmany = 2
	#for x in range(howmany):
		#for y in range(howmany):
			#var px = (((1280) / (howmany+1)) * x)
			#var py = (((720) / (howmany+1)) * y)
			#pixels.push_back(image.get_pixel(px, py))
	#
	#for p in pixels:
		#rgb[0] += p.r
		#rgb[1] += p.g
		#rgb[2] += p.b
		#
	#rgb[0] /= pixels.size()
	#rgb[1] /= pixels.size()
	#rgb[2] /= pixels.size()
	#
	#var light_color = Color(rgb[0],rgb[1],rgb[2],1)
	
	SignalBus.on_light_color.emit(image.get_pixel(1,1))

func _on_finished():
	playRandomVideo()
