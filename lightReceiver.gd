extends OmniLight3D

var l = Color(1,1,1,1)
# Called when the node enters the scene tree for the first time.
func _ready():
	SignalBus.on_light_color.connect(_on_light_color)

func _on_light_color(color:Color):
	light_color = color * 0.5 + l * 0.5
	pass
