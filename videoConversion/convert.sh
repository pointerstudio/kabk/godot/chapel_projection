#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DIR_PREVIOUS="$(pwd)"

DIR_INPUT="$DIR/input"
DIR_OUTPUT="$DIR/output"
DIR_TMP="/tmp/videoconvert"
DIR_TMP_INDIVIDUAL="$DIR_TMP/individual"
DIR_OUTPUT_INDIVIDUAL="$DIR_OUTPUT/individual"
DIR_OUTPUT_MERGED="$DIR_OUTPUT/merged"

mkdir -p "$DIR_INPUT"
mkdir -p "$DIR_OUTPUT"
mkdir -p "$DIR_TMP"
mkdir -p "$DIR_TMP_INDIVIDUAL"
mkdir -p "$DIR_OUTPUT_INDIVIDUAL"
mkdir -p "$DIR_OUTPUT_MERGED"

for v in $DIR_INPUT/*.*; do
    if [ -f "$v" ]; then
        basename=$(basename -- "$v")
        filename="${basename%.*}"
        new_filename="$(echo $filename | sed 's/ //g')"
        extension="${basename##*.}"

        #echo "------------------- $v ----------:"
        #echo "basename: $basename"
        #echo "filename: $filename"
        #echo "new_filename: $new_filename"
        #echo "extension: $extension"
        #echo "found file: $filename"

        has_audio=$(ffprobe "$v" 2>&1 | grep 'Audio')
        duration=$(ffprobe "$v" -show_entries format=duration -v quiet -of csv="p=0" -sexagesimal)
        fps=$(ffprobe -v error -select_streams v -of default=noprint_wrappers=1:nokey=1 -show_entries stream=r_frame_rate "$v")

        fpscmd=""

        qv=5
        qa=1

        echo "$new_filename:   $duration:    $(du -sh $DIR_OUTPUT_INDIVIDUAL/$new_filename.ogv | cut -f1)    $fps"

        if [ "$new_filename" == "alex-r" ];
        then
            fpscmd="-filter:v fps=fps=30"
        fi
        if [ "$new_filename" == "Dongha-ProjectWeek" ];
        then
            duration="00:00:20"
        fi
        if [ "$new_filename" == "info" ];
        then
            fpscmd="-filter:v fps=fps=30"
        fi
        if [ "$new_filename" == "Jin" ];
        then
            fpscmd="-filter:v fps=fps=30"
        fi
        if [ "$new_filename" == "ScreenRecording2024-01-18at10.59.13" ];
        then
            fpscmd="-filter:v fps=fps=30"
        fi
        if [ "$new_filename" == "ScreenRecording2024-01-18at16.16.09" ];
        then
            fpscmd="-filter:v fps=fps=30"
        fi
        if [ "$new_filename" == "Stay" ];
        then
            fpscmd="-filter:v fps=fps=30"
        fi
        if [ "$new_filename" == "michalina1" ];
        then
            duration="00:00:20"
            fpscmd="-filter:v fps=fps=30"
        fi
        if [ "$new_filename" == "michalina2" ];
        then
            duration="00:00:20"
            fpscmd="-filter:v fps=fps=30"
        fi

        audio_cmd=""
        audio_cmd_r=""

        if [ ! -z "$has_audio" ]; then
            #echo "video has audio"
            audio_cmd_r="-ar 44100"
            qa=6
        else
            #echo "video has no audio"
            #echo "create empty audio track"
            audio_cmd="-f lavfi -i anullsrc=channel_layout=stereo:sample_rate=44100"
        fi

        ffmpeg \
            -y \
            $audio_cmd \
            -i "$v" \
            $audio_cmd_r \
            $fps_cmd \
            -c:v libtheora -c:a libvorbis \
            -t "$duration" \
            -vf "scale=1280:720" -q:v $qv -q:a $qa \
            "$DIR_OUTPUT_INDIVIDUAL/$new_filename.ogv"
        #ffmpeg \
            #-y \
            #$audio_cmd \
            #-i "$v" \
            #$audio_cmd_r \
            #$fps_cmd \
            #-c:v libtheora -c:a libvorbis \
            #-t "$duration" \
            #-vf "scale=1:1" -q:v $qv -q:a $qa \
            #"$DIR_OUTPUT_MERGED/$new_filename.ogv"
    else
        echo "found non-file: $v"
    fi
done
